# docker-centos7-tensorflow
docker image to build tensorflow on CentOS-7 x86_64

```
sudo docker build -t docker-centos7-tensorflow .
```

build status:
[![build status](https://gitlab.pasteur.fr/tru/docker-centos7-tensorflow/badges/master/build.svg)](https://gitlab.pasteur.fr/tru/docker-centos7-tensorflow/commits/master)

